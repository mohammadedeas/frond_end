// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false , 
  firebase : { 
    apiKey: "AIzaSyBPpi2pBYCB6Hp63rd4ICaDsJbNbJKP4y8",
    authDomain: "user-b8b4c.firebaseapp.com",
    databaseURL: "https://user-b8b4c.firebaseio.com",
    projectId: "user-b8b4c",
    storageBucket: "user-b8b4c.appspot.com",
    messagingSenderId: "984271394691",
    appId: "1:984271394691:web:3948d965c5d5399c84c8b6",
    measurementId: "G-6L5BKT9H2E"

  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
