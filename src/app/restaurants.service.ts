import { Injectable } from '@angular/core';
import { Restaurant } from './restaurant';
import { Observable } from 'rxjs';
import { Menu } from './menu';
import { Order } from './order';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { RestaurantRating } from './restaurant-rating';

@Injectable({
  providedIn: 'root'
})
export class RestaurantsService {

  constructor(private http: HttpClient) { }

  url="http://localhost:3000/restaurant";
  urlmenu="http://localhost:3000/menu";
  urlorder="http://localhost:3000/orders";
  urlMenuOrder="http://localhost:3000/menu-order";
  urlRating="http://localhost:3000/rating";
  


  
 
  

  getrestfromdb():Observable<Object> {
    return this.http.get(this.url);
  }

 
  getmenudb(rest_id):Observable<Object>{
  return this.http.get(this.urlmenu+"/"+rest_id)
  }



  addRestdb(rest: Restaurant): Observable<Object>{
    const httpOptions = {
      headers: new HttpHeaders({
        "Content-Type": "application/json"
      })
    }
    let body = {
      "restaurant":rest
    }
   return this.http.post(this.url, body, httpOptions);
  }

 

  addMenudb(menu: Menu): Observable<Object>{
    const httpOptions = {
      headers: new HttpHeaders({
        "Content-Type": "application/json"
      })
    }
    let body = {
      "menu":menu
    }
   return this.http.post(this.urlmenu, body, httpOptions);
  }

 

  
 

  

  getordersdb():Observable<Object> {
    return this.http.get(this.urlorder);
  }
  getallorder(cust_id):Observable<Object>{
    return this.http.get(this.urlorder+"/"+cust_id)
    }

  addOrderdb(order: Order): Observable<Object>{
    const httpOptions = {
      headers: new HttpHeaders({
        "Content-Type": "application/json"
      })
    }
    let body = {
      "orders":order
    }
   return this.http.post(this.urlorder, body, httpOptions);
  }

  deleteorderdb(id):Observable<Object>{
    //  console.log("deleted");
    return this.http.delete(this.urlorder+"/"+id);
    
 
   }
  

  

  




  getrestdb(id){
    return this.http.get(this.url+"/"+id);
  }



  

  updaterestdb(rest:Restaurant):Observable<Object>{

    const httpOptions = {
      headers: new HttpHeaders({
        "Content-Type": "application/json"
      })
    }
    let body = {
      "restaurant":rest
    }
   return this.http.put(this.url+"/"+rest.id,body)

  }

  

  getmenudatadb(id){
    return this.http.get(this.urlmenu+"/"+id);
  }

  getmenuorder(id){
    return this.http.get(this.urlMenuOrder+"/"+id);
  }




 


  updatemenudb(menu:Menu):Observable<Object>{

    const httpOptions = {
      headers: new HttpHeaders({
        "Content-Type": "application/json"
      })
    }
    let body = {
      "menu":menu
    }
   return this.http.put(this.urlmenu+"/"+menu.id,body)

  }

  deleterest(id):Observable<Object>{
   return this.http.delete(this.url+"/"+id);
    

  }


  rating(rate: RestaurantRating): Observable<Object>{
    const httpOptions = {
      headers: new HttpHeaders({
        "Content-Type": "application/json"
      })
    }
    let body = {
      "rating":rate
    }
   return this.http.post(this.urlRating, body, httpOptions);
  }

  getrating(rest_id):Observable<Object>{
    return this.http.get(this.urlRating+"/"+rest_id)
    }


    uploadImagerest(data:any):Observable<object>{
      return this.http.post(this.url+'restaurant/SaveFile',data);
  
     }
     
}

 