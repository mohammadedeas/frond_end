import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import{ActivatedRoute} from'@angular/router';
import { Menu } from '../menu';
import { Restaurant } from '../restaurant';
import { RestaurantsService } from '../restaurants.service';
@Component({
  selector: 'app-edit-menu',
  templateUrl: './edit-menu.component.html',
  styleUrls: ['./edit-menu.component.css']
})
export class EditMenuComponent implements OnInit {
  restaurantss:Restaurant[]=[];
  rest:Menu[]=[];
  errorMessage:string;
  editmenu=new FormGroup({
     
        name:new FormControl('') ,
        descr: new FormControl(''),
        id:new FormControl(''),
        image:new FormControl('') ,
        rest_id: new FormControl(''),
        price:new FormControl('')

  })
   
  constructor(private service:RestaurantsService,
    private formBuilder: FormBuilder,private rout:ActivatedRoute) { }

  ngOnInit(): void {
   
 
    this.service.getmenudatadb(this.rout.snapshot.params.id).subscribe((resta:Menu[])=> {
      this.rest = resta;

      

      console.log(JSON.stringify(resta));
      console.log(this.rest[0].name);
      
      this.editmenu=new FormGroup({
            name:new FormControl(this.rest[0].name) ,
            descr: new FormControl(this.rest[0].descr),
            id:new FormControl(this.rest[0].id),
            image:new FormControl(this.rest[0].image) ,
            rest_id: new FormControl(this.rest[0].rest_id),
            price:new FormControl(this.rest[0].price)
           
  
            
      })

      
    },
    (error: any)=> {
      console.log(error);
      this.errorMessage = error;
    }
    )

  }
  onSubmit(){
    

    // this.service.editemenu(this.rout.snapshot.params.id, this.editmenu.value);

    this.service.updatemenudb(this.editmenu.value).subscribe(res=>{
      console.log('update data',res);
       
    },error=>{
      console.log("error2",error);
    })
  };


}
