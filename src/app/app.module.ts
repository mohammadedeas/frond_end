import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
 
 
import {  HttpClientModule} from '@angular/common/http'; 
 
import { RouterModule } from '@angular/router';
 
 
import { AngularFireModule } from 'angularfire2'; 
import {AngularFireDatabaseModule} from  'angularfire2/database'; 
import { environment } from 'src/environments/environment';
import { FormComponent } from './form/form.component';
import { PostDetailsComponent } from './post-details/post-details.component';
import { RestaurantsListComponent } from './restaurants-list/restaurants-list.component';
import { MenuListComponent } from './menu-list/menu-list.component';
import { NavBarComponent } from './nav-bar/nav-bar.component';
import { RestFormComponent } from './rest-form/rest-form.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MenuFormComponent } from './menu-form/menu-form.component';
import { ListOrderComponent } from './list-order/list-order.component';
import { EditRestaurantComponent } from './edit-restaurant/edit-restaurant.component';
import { EditMenuComponent } from './edit-menu/edit-menu.component';


@NgModule({
  declarations: [
    AppComponent,
    
     
    FormComponent,
     
    PostDetailsComponent,
     
    RestaurantsListComponent,
     
    MenuListComponent,
     
    NavBarComponent,
     
    RestFormComponent,
     
    MenuFormComponent,
     
    ListOrderComponent,
     
    EditRestaurantComponent,
     
    EditMenuComponent,
     
   
  ],
  imports: [
    BrowserModule,
    AppRoutingModule ,
    HttpClientModule, 
    FormsModule,
     
    
    ReactiveFormsModule,
    RouterModule.forRoot([
          



    ]) ,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule
  ],
  // providers: [PostService],
  bootstrap: [AppComponent]
})
export class AppModule { }
