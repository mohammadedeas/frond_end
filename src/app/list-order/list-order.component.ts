import { Component, OnInit } from '@angular/core';
import { Menu } from '../menu';
import { Order } from '../order';
import { Restaurant } from '../restaurant';
import { RestaurantsService } from '../restaurants.service';

@Component({
  selector: 'app-list-order',
  templateUrl: './list-order.component.html',
  styleUrls: ['./list-order.component.css']
})
export class ListOrderComponent implements OnInit {
listorder:Order[]=[];
listmenu: Menu[]=[];
listrest:Restaurant[]=[];
list:any[]=[];
omlist:any[]=[];
errorMessage:string;
responseText:string;
custumer_id:number=1;
  constructor(private service :RestaurantsService) { }

  ngOnInit(): void {

    this.service.getallorder(this.custumer_id).subscribe(
      (listorder:Order[])=> {
        this.listorder = listorder;
        // console.log(JSON.stringify(this.restaurants));
        this.listorder.forEach(element => {

          this.service.getmenuorder(element.menu_id).subscribe(
            ( listmenu: Menu[])=> {
              this.listmenu = listmenu;
              let omi:any={order :element,menu:this.listmenu[0],totalprice:element.quantity*this.listmenu[0].price};
              this.omlist.push(omi);
              console.log(JSON.stringify(this.listmenu));
            },
            (error: any)=> {
              console.log(error);
              this.errorMessage = error;
            }
      
          )
          
        });
      },
      (error: any)=> {
        console.log(error);
        this.errorMessage = error;
      }

    )



  //  this.listorder= this.service.getorderlist();
  //  this.listmenu=this.service.allmenu();
  //  this.listrest=this.service.getrestaurants();

  }

  delete(iteem){
    // this.service.deletorder(iteem);

    this.service.deleteorderdb(iteem.order.id).subscribe(
      (data)=> this.responseText = JSON.stringify(data),
      (error)=> this.responseText = error
    )

    let index = this.omlist.indexOf(iteem) ; 
      this.omlist.splice(index,1); 

    console.log(this.responseText);


  }

  

}
