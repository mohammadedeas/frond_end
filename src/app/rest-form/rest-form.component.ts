import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
// import { FormBuilder, ReactiveFormsModule } from '@angular/forms';
import { RestaurantsService } from '../restaurants.service';

@Component({
  selector: 'app-rest-form',
  templateUrl: './rest-form.component.html',
  styleUrls: ['./rest-form.component.css']
})
export class RestFormComponent implements OnInit {

  checkoutForm;
  submitted = false;
  responseText = "";
  
  constructor(
    private service :RestaurantsService,
    private formBuilder: FormBuilder
     )
     {

      this.checkoutForm = this.formBuilder.group({
        name: '',
        city: '',
        raiting: '',
        phone: '',
        image: '',
        lat: '',
        lng: ''

      });

     }

  ngOnInit(): void {
  }

  onSubmit(customerData) {
    
    
    this.service.addRestdb(customerData).subscribe(
      //create
      (data)=> this.responseText = JSON.stringify(data),
      (error)=> this.responseText = error
    )
    this.checkoutForm.reset();

  }
  

}
