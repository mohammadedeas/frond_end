import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Restaurant } from '../restaurant';
import { RestaurantsService } from '../restaurants.service';

@Component({
  selector: 'app-menu-form',
  templateUrl: './menu-form.component.html',
  styleUrls: ['./menu-form.component.css']
})


export class MenuFormComponent implements OnInit {
  checkoutForm;
  restaurants:Restaurant[]=[];
  submitted = false;
  responseText = "";
  errorMessage: String;
  constructor(
    private service :RestaurantsService,
    private formBuilder: FormBuilder, 
    ) { 
      this.checkoutForm = this.formBuilder.group({
        name: '',
        descr: '',
         
        rest_id: '',
        price: '',
        image: ''
        

      });



    }

  ngOnInit(): void {

    this.service.getrestfromdb().subscribe(
      (restaurants :Restaurant[])=> {
        this.restaurants = restaurants;
        console.log(JSON.stringify(this.restaurants));
      },
      (error: any)=> {
        console.log(error);
        this.errorMessage = error;
      }

    )
  }

  onSubmit(customerData) {
  
  
    this.service.addMenudb(customerData).subscribe(
      (data)=> this.responseText = JSON.stringify(data),
      (error)=> this.responseText = error
    )
    this.checkoutForm.reset();




  }

}
 