import { Component, OnInit } from '@angular/core';
import { Restaurant } from '../restaurant';
import { RestaurantsService } from '../restaurants.service';
import { FormBuilder, ReactiveFormsModule } from '@angular/forms';
import { RestaurantRating } from '../restaurant-rating';

@Component({
  selector: 'app-restaurants-list',
  templateUrl: './restaurants-list.component.html',
  styleUrls: ['./restaurants-list.component.css']
})
export class RestaurantsListComponent implements OnInit {
restaurants :Restaurant[]=[];
errorMessage: String;
responseText = "";
responseTextr="";
rrlist:any[]=[];
listrate :any;
checkoutForm;
  constructor(private service:RestaurantsService,private formBuilder: FormBuilder) {
    this.checkoutForm = this.formBuilder.group({
     
      rating: ''
      

    });
   }

  ngOnInit(): void {
     

    this.service.getrestfromdb().subscribe(
      (restaurants :Restaurant[])=> {
        this.restaurants = restaurants;

        this.restaurants.forEach(element => {

          this.service.getrating(element.id).subscribe(
            ( listrate)=> {
              this.listrate = listrate;
              let rri:any={restaurant :element,rating:this.listrate[0].rate};
              this.rrlist.push(rri);
              console.log(JSON.stringify(this.listrate[0].rate));
            },
            (error: any)=> {
              console.log(error);
              this.errorMessage = error;
            }
      
          )
          
        });





      },
      (error: any)=> {
        console.log(error);
        this.errorMessage = error;
      }

    )

    

  }

  delete(rest){
   
    this.service.deleterest(rest.id).subscribe(
      (data)=> this.responseText = JSON.stringify(data),
      (error)=> this.responseText = error
    )

    let index = this.restaurants.indexOf(rest) ; 
      this.restaurants.splice(index,1); 

    console.log(this.responseText);


  }

  rating(r,rest:Restaurant){
    let rate:RestaurantRating={customer_id:1,rating:r.rating,rest_id:rest.id}
   //  orderi.customer_id=1;
   //  orderi.menu_id=iteem.id;
   //  orderi.rest_id=iteem.rest_id;
   //  orderi.quantity=1;
 
      
 
     this.service.rating(rate).subscribe(
       (data)=> this.responseTextr = JSON.stringify(data),
       (error)=> this.responseTextr = error
     )
 
     console.log(this.responseTextr)
   }

}
