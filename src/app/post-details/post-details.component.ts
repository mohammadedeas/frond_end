import { Component, OnInit } from '@angular/core';
import { UserpostService } from '../userpost.service';

@Component({
  selector: 'app-post-details',
  templateUrl: './post-details.component.html',
  styleUrls: ['./post-details.component.css']
})
export class PostDetailsComponent implements OnInit {
post:any=[];
  constructor(private service : UserpostService) { }

  ngOnInit(): void {
    this.service.gettingPost() 
    .subscribe(response => {
     
      this.post = response ;  
    })
  }

  createPost(input :HTMLInputElement){ 

    
    let post:any = {body : input.value }  ; 
    
    this.service.creatPost(post)
   .subscribe(response => {
      console.log(response); 
      post.body =  response ;  
      this.post.splice(0,0,post);
       
    })

  }  
  onUpdate(post){
   
      this.service.updatePost(post)
      .subscribe(response => {
        console.log(response); 
      })
  } 
  onRemove(post){
   
    this.service.deletPost(post)
    .subscribe(response => { 
      let index = this.post.indexOf(post) ; 
      this.post.splice(index,1); 

    })
  }

}
