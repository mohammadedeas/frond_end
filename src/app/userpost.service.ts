import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UserpostService {

  
  url = "https://jsonplaceholder.typicode.com/comments" ;  
  constructor(private http : HttpClient) {  


  } 

  gettingPost(){ 
    return this.http.get( this.url) ; 

  } 

  creatPost(post){ 
    return this.http.post(this.url, JSON.stringify(post)) ; 

  } 

  updatePost(post){ 
    return  this.http.patch(this.url + "/"+post.id, JSON.stringify(post.body="updated post")) ; 

  } 

  deletPost(post){ 
    return this.http.delete(this.url + "/"+post.id) ; 

  }
}
