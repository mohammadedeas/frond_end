import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import{ActivatedRoute} from'@angular/router';
import { Restaurant } from '../restaurant';
import { RestaurantsService } from '../restaurants.service';

@Component({
  selector: 'app-edit-restaurant',
  templateUrl: './edit-restaurant.component.html',
  styleUrls: ['./edit-restaurant.component.css']
})
export class EditRestaurantComponent implements OnInit {
    rest:Restaurant[];
    errorMessage:string;
    editrest=new FormGroup({
      id: new FormControl(''),
          name:new FormControl('') ,
          city: new FormControl(''),
          phone:new FormControl(''),
          image:new FormControl('') ,
          lat: new FormControl(''),
          lng:new FormControl('')

  })
   

  constructor(private service:RestaurantsService,
    private formBuilder: FormBuilder,private rout:ActivatedRoute ) { }

  ngOnInit(): void {

    // this.rest = this.service.getdatadb(this.rout.snapshot.params.id)

    this.service.getrestdb(this.rout.snapshot.params.id).subscribe((resta:Restaurant[])=> {
        this.rest = resta;

        

        console.log(JSON.stringify(resta));
        console.log(this.rest[0].name);
        this.editrest=new FormGroup({
          id: new FormControl(this.rest[0].id),
          name:new FormControl(this.rest[0].name) ,
          city: new FormControl(this.rest[0].city),
          phone:new FormControl(this.rest[0].phone),
          image:new FormControl(this.rest[0].image) ,
          lat: new FormControl(this.rest[0].lat),
          lng:new FormControl(this.rest[0].lng)
             
    
              
        })

        
      },
      (error: any)=> {
        console.log(error);
        this.errorMessage = error;
      }

    )

       
    }
    onSubmit(){

      // this.service.editerest(this.rout.snapshot.params.id, this.editrest.value);

      this.service.updaterestdb(this.editrest.value).subscribe(res=>{
        console.log('update data',res);
         
      },error=>{
        console.log("error2",error);
      })
    };

    
  }


