import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EditMenuComponent } from './edit-menu/edit-menu.component';
import { EditRestaurantComponent } from './edit-restaurant/edit-restaurant.component';
import { ListOrderComponent } from './list-order/list-order.component';
import { MenuFormComponent } from './menu-form/menu-form.component';
import { MenuListComponent } from './menu-list/menu-list.component';
import { RestFormComponent } from './rest-form/rest-form.component';
import { RestaurantsListComponent } from './restaurants-list/restaurants-list.component';


const routes: Routes = [
  {path:'menu/:id',component:MenuListComponent},
  {path:'menul',component:MenuListComponent},
  {path:'rstform',component:RestFormComponent},
  {path:'rstlist',component:RestaurantsListComponent},
 
  {path:'menuform',component:MenuFormComponent},
  {path:'orderlist',component:ListOrderComponent},
  {path:'editrestaurant/:id',component:EditRestaurantComponent},
  {path:'editmenu/:id',component:EditMenuComponent},


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
