import { Component, Input, OnInit } from '@angular/core';
import { Menu } from '../menu';
import { Restaurant } from '../restaurant';
import { RestaurantsService } from '../restaurants.service';
import{ActivatedRoute} from'@angular/router';
import { Order } from '../order';
import { FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-menu-list',
  templateUrl: './menu-list.component.html',
  styleUrls: ['./menu-list.component.css']
})
export class MenuListComponent implements OnInit {
  checkoutForm;
  constructor(private service :RestaurantsService, private formBuilder: FormBuilder,private rout:ActivatedRoute) { 
    this.checkoutForm = this.formBuilder.group({
     
      rating: ''
      

    });
  }

  @Input()
  rest:Restaurant;


  menuitems:Menu[]=[];
  responseText:string;
  errorMessage: String;
  
  ngOnInit(): void {
    // this.menuitems=this.sercvice.getmenu(this.rout.snapshot.params.id);

    this.service.getmenudb(this.rout.snapshot.params.id).subscribe(
      ( menuitems:Menu[])=> {
        this.menuitems = menuitems;
        console.log(JSON.stringify(this.menuitems));
      },
      (error: any)=> {
        console.log(error);
        this.errorMessage = error;
      }

    )

  }

  order(f,iteem:Menu){
   let orderi:Order={customer_id:1,quantity:f.rating,menu_id:iteem.id,rest_id:iteem.rest_id}
  //  orderi.customer_id=1;
  //  orderi.menu_id=iteem.id;
  //  orderi.rest_id=iteem.rest_id;
  //  orderi.quantity=1;

     

    this.service.addOrderdb(orderi).subscribe(
      (data)=> this.responseText = JSON.stringify(data),
      (error)=> this.responseText = error
    )
    console.log("order status")

    console.log(this.responseText)
  }



}
